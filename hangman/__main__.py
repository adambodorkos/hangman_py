import hangman.util as hangman_util
from distutils.util import strtobool


HIGH_SCORE = 0


def play():
    global HIGH_SCORE

    game = hangman_util.create_3d_hubs_game()

    while not game.is_won() and not game.is_lost():
        print(f'\t{game.create_hint()}')
        print(f'High score: {HIGH_SCORE}')
        print(f'Your score: {game.get_current_score()}')

        guessed_letter = input('Guess a letter: ').strip()
        game.guess(guessed_letter)

        print('-' * 80)

    if game.is_won():
        print('Congratulations, you won!')
    else:
        print('You lost!')

    score = game.get_current_score()
    if score > HIGH_SCORE:
        print(f'YOU SET A NEW HIGH SCORE OF {score}!')
        HIGH_SCORE = score


while True:
    play()

    if not strtobool(input('Play again? ')):
        break

    print('=' * 80)
