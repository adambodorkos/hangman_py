from hangman.game import Game
import random


def create_random_game(word_choices, game_config=None):
    word = random.choice(word_choices)
    return Game(word, game_config)


def create_3d_hubs_game(game_config=None):
    word_choices = ['3dhubs', 'marvin', 'print', 'filament', 'order', 'layer']
    return create_random_game(word_choices, game_config)
