

class GameFinishedException(Exception):
    pass


class GameConfig(object):

    DEFAULT_PLACEHOLDER = '_'
    DEFAULT_NUM_MAX_WRONG_GUESSES = 5

    def __init__(self, **kwargs):
        self.__placeholder = self.DEFAULT_PLACEHOLDER
        self.__num_max_wrong_guesses = self.DEFAULT_NUM_MAX_WRONG_GUESSES

        for key, value in kwargs.items():
            setattr(self, key, value)

    @property
    def placeholder(self):
        return self.__placeholder

    @placeholder.setter
    def placeholder(self, placeholder):
        self.__placeholder = str(placeholder)

        if len(self.__placeholder) == 0:
            raise ValueError('The placeholder cannot be empty')

    @property
    def num_max_wrong_guesses(self):
        return self.__num_max_wrong_guesses

    @num_max_wrong_guesses.setter
    def num_max_wrong_guesses(self, num_max_wrong_guesses):
        self.__num_max_wrong_guesses = int(num_max_wrong_guesses)

        if self.__num_max_wrong_guesses < 0:
            raise ValueError(f'The number of maximum wrong guesses ({self.__num_max_wrong_guesses}) '
                             f'must be a positive number')


class Game(object):

    def __init__(self, word, game_config=None):
        self.__word = str(word).lower()
        self.__cfg = game_config or GameConfig()
        self.__letters_guessed = set()
        self.__do_guess = self.__do_guess_when_game_in_progress
        self.__score = 0

        self.__validate()

    def guess(self, letter):
        if len(letter) != 1:
            raise ValueError('A guess must be a single letter')

        self.__do_guess(letter.lower())

        if self.is_won() or self.is_lost():
            self.__finish_game()

    def get_current_score(self):
        return self.__score

    def create_hint(self):
        return ' '.join(c if c in self.__letters_guessed else self.__cfg.placeholder for c in self.__word)

    def is_won(self):
        return all(c in self.__letters_guessed for c in self.__word)

    def is_lost(self):
        return self.__count_wrong_guesses() > self.__cfg.num_max_wrong_guesses

    def __validate(self):
        if len(self.__word) == 0:
            raise ValueError('The word cannot be an empty string')

    def __count_wrong_guesses(self):
        return sum(1 for c in self.__letters_guessed if c not in self.__word)

    def __finish_game(self):
        self.__do_guess = self.__do_guess_when_game_finished

    def __do_guess_when_game_in_progress(self, letter):
        if letter in self.__letters_guessed:
            self.__score -= 1
        else:
            self.__letters_guessed.add(letter)
            self.__score += 1 if letter in self.__word else -1

    def __do_guess_when_game_finished(self, letter):
        raise GameFinishedException()
