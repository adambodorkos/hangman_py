import setuptools

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='hangman',
    version='1.0.0',
    author='Adam Bodorkos',
    author_email='adam.bodorkos@gmail.com',
    description='A hangman game',
    long_description=long_description,
    packages=setuptools.find_packages()
)
