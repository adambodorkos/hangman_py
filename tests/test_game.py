from hangman.game import Game, GameConfig, GameFinishedException
import unittest


class TestGameConfig(unittest.TestCase):

    def test_invalid_placeholder(self):
        under_test = GameConfig()

        with self.assertRaises(ValueError):
            under_test.placeholder = ''

        with self.assertRaises(ValueError):
            GameConfig(placeholder='')

    def test_invalid_num_max_wrong_guesses(self):
        under_test = GameConfig()

        with self.assertRaises(ValueError):
            under_test.num_max_wrong_guesses = -1

        with self.assertRaises(ValueError):
            GameConfig(num_max_wrong_guesses=-1)


class TestGame(unittest.TestCase):

    def test_invalid_word(self):
        with self.assertRaises(ValueError):
            Game(word='')

    def test_no_guesses(self):
        under_test = Game(word='foobar')

        self.assertEqual('_ _ _ _ _ _', under_test.create_hint())
        self.assertEqual(0, under_test.get_current_score())
        self.assertFalse(under_test.is_won())
        self.assertFalse(under_test.is_lost())

    def test_custom_placeholder(self):
        cfg = GameConfig(placeholder='X')
        under_test = Game(word='foobar', game_config=cfg)

        self.assertEqual('X X X X X X', under_test.create_hint())

    def test_good_guess(self):
        under_test = Game(word='foobar')

        under_test.guess('o')

        self.assertEqual('_ o o _ _ _', under_test.create_hint())
        self.assertEqual(1, under_test.get_current_score())
        self.assertFalse(under_test.is_won())
        self.assertFalse(under_test.is_lost())

    def test_bad_guess(self):
        under_test = Game(word='foobar')

        under_test.guess('z')

        self.assertEqual('_ _ _ _ _ _', under_test.create_hint())
        self.assertEqual(-1, under_test.get_current_score())
        self.assertFalse(under_test.is_won())
        self.assertFalse(under_test.is_lost())

    def test_guessing_same_letter_again_decreases_score(self):
        under_test = Game(word='hello')

        under_test.guess('l')

        self.assertEqual('_ _ l l _', under_test.create_hint())
        self.assertEqual(1, under_test.get_current_score())
        self.assertFalse(under_test.is_won())
        self.assertFalse(under_test.is_lost())

        under_test.guess('l')

        self.assertEqual('_ _ l l _', under_test.create_hint())
        self.assertEqual(0, under_test.get_current_score())

    def test_game_won(self):
        cfg = GameConfig(num_max_wrong_guesses=0)
        under_test = Game(word='foobar', game_config=cfg)

        under_test.guess('b')
        under_test.guess('f')
        under_test.guess('r')
        under_test.guess('o')
        under_test.guess('a')

        self.assertEqual('f o o b a r', under_test.create_hint())
        self.assertEqual(5, under_test.get_current_score())
        self.assertTrue(under_test.is_won())
        self.assertFalse(under_test.is_lost())

    def test_too_many_bad_guesses(self):
        cfg = GameConfig(num_max_wrong_guesses=4)
        under_test = Game(word='abcdefghijklmnop', game_config=cfg)

        # good guesses
        under_test.guess('a')
        under_test.guess('b')
        under_test.guess('c')
        under_test.guess('d')
        under_test.guess('e')

        self.assertEqual(5, under_test.get_current_score())
        self.assertFalse(under_test.is_won())
        self.assertFalse(under_test.is_lost())

        # bad guesses
        under_test.guess('q')
        under_test.guess('r')
        under_test.guess('s')
        under_test.guess('t')

        self.assertEqual(1, under_test.get_current_score())
        self.assertFalse(under_test.is_won())
        self.assertFalse(under_test.is_lost())

        # one more bad guess ends the game
        under_test.guess('u')

        self.assertEqual(0, under_test.get_current_score())
        self.assertFalse(under_test.is_won())
        self.assertTrue(under_test.is_lost())

    def test_game_already_finished_raises_exception(self):
        under_test = Game(word='a')
        under_test.guess('a')

        with self.assertRaises(GameFinishedException):
            under_test.guess('b')


if __name__ == '__main__':
    unittest.main()
